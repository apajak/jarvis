// Configure marked pour utiliser highlight.js
marked.setOptions({
    renderer: new marked.Renderer(),
    highlight: function(code, lang) {
        // Vérifie si le langage spécifié est pris en charge par highlight.js
        const validLanguage = hljs.getLanguage(lang) ? lang : 'plaintext';
        // Applique le highlighting au code avec le langage spécifié
        return hljs.highlight(code, { language: validLanguage }).value;
    },
    langPrefix: 'language-', // Cette classe est ajoutée au bloc <code>
    pedantic: false,
    gfm: true,
    breaks: true,
    sanitize: false,
    smartLists: true,
    smartypants: false,
    xhtml: false
});



document.addEventListener('DOMContentLoaded', () => {
    const sendMessageButton = document.getElementById('send-message');
    const messageInput = document.getElementById('text-message');
    const messageThread = document.getElementById('message-thread');
    const toggleButton = document.getElementById('toggle-config');
    const configPanel = document.getElementById('config-panel');

    sendMessageButton.addEventListener('click', () => {
        const message = messageInput.value.trim();
        if (message) {
            addMessage('user-message', message);
            messageInput.value = ''; // Clear input after sending

            // Simulate a response after a short delay
            setTimeout(() => {
                addMessage('response-message', "# Ceci est la réponse:\n```python\nprint('Hello, world!')\nreturn True\n```");
            }, 1000);
        }
    });
    function addCopyButtonsToCodeBlocks(containerElement) {
        containerElement.querySelectorAll('pre code').forEach((codeBlock) => {
            const copyButton = document.createElement('button');
            copyButton.textContent = 'Copier';
            copyButton.className = 'copy-code-button'; // Utilisez cette classe pour le style

            copyButton.onclick = function() {
                navigator.clipboard.writeText(codeBlock.innerText).then(() => {
                    console.log('Code copié avec succès !');
                    copyButton.textContent = 'Copié !';
                    setTimeout(() => copyButton.textContent = 'Copier', 2000);
                }, (err) => {
                    console.error('Erreur lors de la copie :', err);
                });
            };

            const preElement = codeBlock.parentNode;
            const codeContainer = document.createElement('div');
            codeContainer.className = 'code-container'; // Pour le positionnement relatif

            // Déplace le bloc pre dans le conteneur et ajoute le bouton de copie
            preElement.before(codeContainer);
            codeContainer.appendChild(preElement);
            codeContainer.appendChild(copyButton);
        });
    }


    function addMessage(className, text) {
        const div = document.createElement('div');
        div.className = `message ${className}`;
        const html = marked.parse(text);
        div.innerHTML = html;

        messageThread.appendChild(div);

        // Applique le highlighting aux blocs de code dans le message ajouté
        div.querySelectorAll('pre code').forEach((block) => {
            hljs.highlightElement(block);
        });

        // Ajoute des boutons de copie après avoir appliqué le highlighting
        addCopyButtonsToCodeBlocks(div);

        // Scroll to the bottom to show the newest message
        messageThread.scrollTop = messageThread.scrollHeight;
    }

    toggleButton.addEventListener('click', () => {
        configPanel.classList.toggle('open');
        // Ajuste la position du bouton de basculement en fonction de l'état du panneau
        toggleButton.style.right = configPanel.classList.contains('open') ? '300px' : '0';
    });



});
